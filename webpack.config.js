const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const TARGET_ENV = process.env.npm_lifecycle_event === 'build' ? 'production' : 'development';

const commonConfig = {
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[hash].js',
  },

  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.js', '.jsx'],
  },

  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader?modules&localIdentName=[name]_[local]_[hash:base64:5]',
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-0'],
        },
      },
      {
        test: /\.(eot|ttf|woff|woff2|svg)$/,
        loader: 'file-loader',
      },
      {
        test: /\.(jpg|png|webp)$/,
        loader: 'file-loader',
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '/index.html'),
      inject: 'body',
      filename: 'index.html',
    }),
  ],
};

if (TARGET_ENV === 'development') {
  console.log('webpack >>> dev mode');
  module.exports = merge.smart(commonConfig, {
    entry: [
      'webpack-dev-server/client',
      path.join(__dirname, 'app/app.js'),
    ],

    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          loader: 'babel',
          query: {
            presets: ['react', 'es2015', 'stage-0', 'react-hmre'],
          },
        },
      ],
    },

    devServer: {
      inline: true,
      progress: true,
    },
  });
}

if (TARGET_ENV === 'production') {
  console.log('webpack >>> prod build');
  module.exports = merge.smart(commonConfig, {
    entry: path.join(__dirname, 'app/app.js'),

    plugins: [
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        minimize: true,
        compressor: { warnings: false },
      }),
    ],
  });
}